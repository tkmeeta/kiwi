package com.xjtucrafter.kiwi.order.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * User: Dash
 * Date: 4/23/16 Time: 4:38 PM
 */
public class ItemsUtil {
    public static String ItemsToJson(List<Integer> itemsList){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(itemsList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Integer> jsonToItems(String itemsJson){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Integer[] itemses = objectMapper.readValue(itemsJson, Integer[].class);
            return Arrays.asList(itemses);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
