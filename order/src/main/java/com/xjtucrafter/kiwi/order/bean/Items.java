package com.xjtucrafter.kiwi.order.bean;

import java.io.Serializable;

/**
 * User: Dash
 * Date: 4/22/16 Time: 10:29 AM
 */
public class Items implements Serializable{
    private int itemID;
    private String itemName;
    private double itemPrice;
    private String itemBelong;
    private String itemPicUrl;
    private String itemDesc;



    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemBelong() {
        return itemBelong;
    }

    public void setItemBelong(String itemBelong) {
        this.itemBelong = itemBelong;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }


}
