package com.xjtucrafter.kiwi.order.bean;


import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * User: Dash
 * Date: 4/22/16 Time: 10:29 AM
 */
@Entity
@Table(name = "ord")
public class Orders implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderID;

    @Temporal(TemporalType.TIMESTAMP)
    private Date orderCreateTime;

    @Column(length = 1023)
    private String orderStatus;

    private String itemsJson;

    @Transient
    private List<Integer> itemsList;


    private int merchantId;


    private int accountId;


    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(Date orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getItemsJson() {
        return itemsJson;
    }

    public void setItemsJson(String itemsJson) {
        this.itemsJson = itemsJson;
    }

    public List<Integer> getItemsList() {
        return itemsList;
    }

    public void setItemsList(List<Integer> itemsList) {
        this.itemsList = itemsList;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
}
