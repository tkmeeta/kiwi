package com.xjtucrafter.kiwi.order.service;

import com.xjtucrafter.kiwi.order.bean.Orders;

import java.util.List;

/**
 * User: Dash
 * Date: 4/22/16 Time: 1:48 PM
 */
public interface OrderService {
    List<Orders> getOrderByAccount(int accountId);
    List<Orders> getOrderByMerchant(int merchantId);
    List<Orders> getOrderByMerchantWithStatus(int merchantId);
    List<Orders> getOrderByAccountWithStatus(int accountId);
    Orders getOrderByCode(int orderId);
    int addPurchaseCarOrder(Orders order);
    boolean addPurchasedOrder(Orders order);
    boolean deleteOrder(int orderId);
    boolean updateOrder(Orders order);
}
