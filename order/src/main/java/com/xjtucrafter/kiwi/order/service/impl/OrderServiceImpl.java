package com.xjtucrafter.kiwi.order.service.impl;

import com.xjtucrafter.kiwi.order.bean.Orders;
import com.xjtucrafter.kiwi.order.dao.OrderDao;
import com.xjtucrafter.kiwi.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: Dash
 * Date: 4/22/16 Time: 1:48 PM
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public List<Orders> getOrderByAccount(int accountId) {
        return orderDao.getOrderByAccount(accountId);
    }

    @Override
    public List<Orders> getOrderByMerchant(int merchantId) {
        return orderDao.getOrderByMerchant(merchantId);
    }

    @Override
    public List<Orders> getOrderByMerchantWithStatus(int merchantId) {
        String status = "unpayed";
        return orderDao.getOrderByMerchant(merchantId, status);

    }

    @Override
    public List<Orders> getOrderByAccountWithStatus(int accountId) {
        String status = "unpayed";
        return orderDao.getOrderByAccount(accountId, status);
    }

    @Override
    public Orders getOrderByCode(int orderId) {

        return orderDao.getOrderByCode(orderId);

    }

    @Override
    @Transactional
    public int addPurchaseCarOrder(Orders order) {
        try {
            return orderDao.addOrder(order);
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    @Transactional
    public boolean addPurchasedOrder(Orders order) {
        return orderDao.addPurchasedOrder(order) > 0;
    }

    @Override
    @Transactional
    public boolean deleteOrder(int orderId) {
        return orderDao.deleteOrder(orderId) > 0;
    }

    @Override
    @Transactional
    public boolean updateOrder(Orders order) {
        return orderDao.updateOrder(order) > 0;
    }
}
