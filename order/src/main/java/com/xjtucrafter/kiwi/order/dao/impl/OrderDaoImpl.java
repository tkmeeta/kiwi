package com.xjtucrafter.kiwi.order.dao.impl;

import com.xjtucrafter.kiwi.order.bean.Orders;
import com.xjtucrafter.kiwi.order.dao.OrderDao;
import com.xjtucrafter.kiwi.order.util.ItemsUtil;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

/**
 * User: Dash
 * Date: 4/22/16 Time: 1:49 PM
 */
@Repository("orderDao")
public class OrderDaoImpl implements OrderDao{
    @PersistenceContext(name = "unitName")
    private EntityManager entityManager;

    private void appendItemsList(List<Orders> ordersList) {
        Iterator it = ordersList.iterator();
        while(it.hasNext()){
            Orders temp = (Orders) it.next();
            temp.setItemsList(ItemsUtil.jsonToItems(temp.getItemsJson()));
        }

    }

    @Override
    public List<Orders> getOrderByAccount(int accountId) {
        String jpql = "select orders from Orders orders where orders.accountId=:account";
        List<Orders> ordersList = entityManager.createQuery(jpql)
                .setParameter("account", accountId)
                .getResultList();
        appendItemsList(ordersList);
        return ordersList;
    }



    @Override
    public List<Orders> getOrderByMerchant(int merchantId) {
        String jpql = "select orders from Orders orders where orders.merchantId=:merchant";
        List<Orders> ordersList = entityManager.createQuery(jpql)
                .setParameter("merchant", merchantId)
                .getResultList();
        appendItemsList(ordersList);
        return ordersList;
    }

    @Override
    public List<Orders> getOrderByMerchant(int merchantId, String orderStatus) {
        String jpql = "select orders from Orders orders where " +
                "orders.merchantId=:merchant and orders.orderStatus=:status";
        List<Orders> ordersList = entityManager.createQuery(jpql)
                .setParameter("merchant", merchantId)
                .setParameter("status", orderStatus)
                .getResultList();
        appendItemsList(ordersList);
        return ordersList;
    }

    @Override
    public List<Orders> getOrderByAccount(int accountId, String orderStatus) {
        String jpql = "select orders from Orders orders where " +
                "orders.accountId=:account and orders.orderStatus=:status";
        List<Orders> ordersList = entityManager.createQuery(jpql)
                .setParameter("account", accountId)
                .setParameter("status", orderStatus)
                .getResultList();
        appendItemsList(ordersList);
        return ordersList;
    }

    @Override
    public Orders getOrderByCode(int orderId) {
        return entityManager.find(Orders.class, orderId);
    }

    @Override
    public int addOrder(Orders order) throws Exception{
        entityManager.persist(order);
        return order.getOrderID();
    }

    @Override
    public int addPurchasedOrder(Orders order) {
        Orders temp = entityManager.find(Orders.class, order.getOrderID());
        temp.setOrderStatus(order.getOrderStatus());
        entityManager.persist(temp);
        return temp.getOrderID();
    }

    @Override
    public int deleteOrder(int orderId) {
        String jpql = "delete from Orders ord where ord.orderID=:id";
        int result = entityManager.createQuery(jpql)
                .setParameter("id", orderId)
                .executeUpdate();
        return result;
    }

    @Override
    public int updateOrder(Orders order) {
        Orders temp = entityManager.find(Orders.class, order.getOrderID());
        temp.setItemsList(order.getItemsList());
        temp.setItemsJson(order.getItemsJson());
        entityManager.persist(temp);
        return temp.getOrderID();
    }
}
