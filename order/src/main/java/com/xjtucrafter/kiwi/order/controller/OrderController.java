package com.xjtucrafter.kiwi.order.controller;

import com.xjtucrafter.kiwi.order.bean.Orders;
import com.xjtucrafter.kiwi.order.service.OrderService;
import com.xjtucrafter.kiwi.order.util.ItemsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * User: Dash
 * Date: 4/22/16 Time: 11:35 AM
 */
@Controller
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService service;

    /**
     * 获得用户所有的订单
     */
    @RequestMapping(value = "/account/{account_id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Orders> getOrderByAccount(@PathVariable("account_id") int accountId) {
        try {
            return service.getOrderByAccount(accountId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 获得商家的所有订单
     */
    @RequestMapping(value = "/merchant/{merchant_id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Orders> getOrderByMerchant(@PathVariable("merchant_id") int merchantId) {
        try{
            return service.getOrderByMerchant(merchantId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 获得商家下未支付的所有订单
     */
    @RequestMapping(value = "/merchant/unpayed/{merchant_id}", method = RequestMethod.GET)
    @ResponseBody
    private List<Orders> getOrderByMerchantWithStatus(@PathVariable("merchant_id") int merchantId) {
        try{
            return service.getOrderByMerchantWithStatus(merchantId);
        }catch (Exception e){
            return null;
        }
    }


    /**
     * 获得用户下未支付的所有订单
     */
    @RequestMapping(value = "/account/unpayed/{account_id}", method = RequestMethod.GET)
    @ResponseBody
    private List<Orders> getOrderByAccountWithStatus(@PathVariable("account_id") int accountId) {
        try {
            return service.getOrderByAccountWithStatus(accountId);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 通过订单号获取订单
     */
    @RequestMapping(value = "/{order_id}", method = RequestMethod.GET)
    @ResponseBody
    private Orders getOrderByCode(@PathVariable("order_id") int orderId) {

        try {
            return service.getOrderByCode(orderId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 添加购物车订单
     * 未支付的订单
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public int addPurchaseCarOrder(HttpServletRequest request) {
        try {
            int accountId = Integer.parseInt(request.getParameter("account_id"));
            int merchantId = Integer.parseInt(request.getParameter("merchant_id"));
            String[] items = request.getParameterValues("ordered_items");
            String orderStatus = "unpayed";
            Date orderCreateTime = new Date();

            List<Integer> itemsList = new ArrayList<Integer>();

            for (String itemId : items) {
                itemsList.add(Integer.parseInt(itemId));
            }


            Orders order = new Orders();
            order.setOrderStatus(orderStatus);
            order.setOrderCreateTime(orderCreateTime);
            order.setAccountId(accountId);
            order.setMerchantId(merchantId);
            order.setItemsList(itemsList);
            order.setItemsJson(ItemsUtil.ItemsToJson(itemsList));

            return service.addPurchaseCarOrder(order);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 支付订单
     */
    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    @ResponseBody
    public boolean addPurchasedOrder(HttpServletRequest request) {
        try {
            int orderId = Integer.parseInt(request.getParameter("order_id"));
            String orderStatus = "payed";
            Orders order = new Orders();
            order.setOrderID(orderId);
            order.setOrderStatus(orderStatus);
            return service.addPurchasedOrder(order);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 删除订单
     */
    @RequestMapping(value = "/{order_id}", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean deleteOrder(@PathVariable("order_id") int orderId) {
        try {
            return service.deleteOrder(orderId);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 修改订单
     */

    @RequestMapping(value = "/update/{order_id}", method = RequestMethod.POST)
    @ResponseBody
    public boolean updateOrder(@PathVariable("order_id") int orderId,
                               HttpServletRequest request) {
        try {
            String[] items = request.getParameterValues("ordered_items");

            List<Integer> itemsList = new ArrayList<Integer>();
            for (String itemId : items) {
                itemsList.add(Integer.parseInt(itemId));
            }

            Orders order = new Orders();
            order.setOrderID(orderId);
            order.setItemsList(itemsList);
            order.setItemsJson(ItemsUtil.ItemsToJson(itemsList));

            return service.updateOrder(order);
        } catch (Exception e) {
            return false;
        }
    }
}


