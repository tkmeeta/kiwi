package com.xjtucrafter.kiwi.order.dao;

import com.xjtucrafter.kiwi.order.bean.Orders;

import java.util.List;

/**
 * User: Dash
 * Date: 4/22/16 Time: 1:49 PM
 */
public interface OrderDao {
    List<Orders> getOrderByAccount(int accountId);

    List<Orders> getOrderByMerchant(int merchantId);

    List<Orders> getOrderByMerchant(int merchantId, String orderStatus);

    List<Orders> getOrderByAccount(int accountId, String orderStatus);

    Orders getOrderByCode(int orderId);

    int addOrder(Orders order) throws Exception;

    int addPurchasedOrder(Orders order);

    int deleteOrder(int orderId);

    int updateOrder(Orders order);
}
