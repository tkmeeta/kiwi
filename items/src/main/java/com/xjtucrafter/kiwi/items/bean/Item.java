package com.xjtucrafter.kiwi.items.bean;


import javax.persistence.*;
import java.io.Serializable;

/**
 * User: Dash
 * Date: 4/19/16 Time: 6:06 PM
 */
@Entity
@Table(name="items")
public class Item implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int itemID;
    private String itemName;
    private double itemPrice;
    private String itemBelong;
    private String itemPicUrl;
    private String itemDesc;

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemBelong() {
        return itemBelong;
    }

    public void setItemBelong(String itemBelong) {
        this.itemBelong = itemBelong;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }
}
