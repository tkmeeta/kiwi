package com.xjtucrafter.kiwi.items.controller;

import com.xjtucrafter.kiwi.items.bean.Item;
import com.xjtucrafter.kiwi.items.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * User: Dash
 * Date: 4/19/16 Time: 5:58 PM
 */
@Controller
@RequestMapping("/items")
public class ItemsController {

    @Autowired
    private ItemService service;

    /**
     * 添加菜品
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public boolean itemAdd(HttpServletRequest request) {
        String itemName = request.getParameter("item_name");
        double itemPrice = Double.parseDouble(request.getParameter("item_price"));
        String itemBelong = request.getParameter("item_belong");
        String itemDesc = request.getParameter("item_desc");
        String itemPicUrl = request.getParameter("item_pic_url");

        Item item = new Item();
        item.setItemName(itemName);
        item.setItemPrice(itemPrice);
        item.setItemBelong(itemBelong);
        item.setItemDesc(itemDesc);
        item.setItemPicUrl(itemPicUrl);

        return service.itemAdd(item);

    }

    /**
     * 查看单一菜品
     */
    @RequestMapping(value = "/{item_name}", method = RequestMethod.GET)
    @ResponseBody
    public Item itemQuery(@PathVariable("item_name") String itemName) {
        return service.itemQuery(itemName);
    }

    /**
     * 批量查看
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> itemBatchQuery() {
        return service.itemBatchQuery();
    }

    /**
     * 删除菜品
     */
    @RequestMapping(value = "/{item_id}", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean itemDelete(@PathVariable("item_id") int itemId) {
        return service.itemDelete(itemId);
    }

    /**
     * 修改菜品
     */
    @RequestMapping(value = "/update/{item_id}", method = RequestMethod.POST)
    @ResponseBody
    public boolean itemUpdate(@PathVariable("item_id") int itemId, HttpServletRequest request) {
        String itemName = request.getParameter("item_name");
        double itemPrice = Double.parseDouble(request.getParameter("item_price"));
        String itemBelong = request.getParameter("item_belong");
        String itemDesc = request.getParameter("item_desc");
        String itemPicUrl = request.getParameter("item_pic_url");


        Item item = new Item();
        item.setItemID(itemId);
        item.setItemName(itemName);
        item.setItemPrice(itemPrice);
        item.setItemBelong(itemBelong);
        item.setItemDesc(itemDesc);
        item.setItemPicUrl(itemPicUrl);

        return service.itemUpdate(item);
    }
}
