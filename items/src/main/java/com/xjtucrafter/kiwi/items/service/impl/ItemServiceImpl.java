package com.xjtucrafter.kiwi.items.service.impl;

import com.xjtucrafter.kiwi.items.bean.Item;
import com.xjtucrafter.kiwi.items.dao.ItemDao;
import com.xjtucrafter.kiwi.items.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * User: Dash
 * Date: 4/19/16 Time: 9:20 PM
 */
@Service("itemService")
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;

    @Override
    @Transactional
    public boolean itemAdd(Item item) {
        int result = itemDao.itemAdd(item);
        if (result >= 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Item itemQuery(String itemName) {
        try {
            return itemDao.itemQuery(itemName);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Item> itemBatchQuery() {
        try {
            return itemDao.itemBatchQuery();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional
    public boolean itemDelete(int itemId) {
        int result = itemDao.itemDelete(itemId);
        if (result > 0){
            return true;
        }else{
            return false;
        }
    }

    @Override
    @Transactional
    public boolean itemUpdate(Item item) {
        int result = itemDao.itemUpdate(item);
        if (result > 0){
            return true;
        }else{
            return false;
        }
    }
}
