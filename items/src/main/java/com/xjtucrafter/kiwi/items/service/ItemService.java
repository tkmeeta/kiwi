package com.xjtucrafter.kiwi.items.service;

import com.xjtucrafter.kiwi.items.bean.Item;

import java.util.List;

/**
 * User: Dash
 * Date: 4/19/16 Time: 9:20 PM
 */
public interface ItemService {
    boolean itemAdd(Item item);
    Item itemQuery(String itemName);
    List<Item> itemBatchQuery();
    boolean itemDelete(int itemId);
    boolean itemUpdate(Item item);

}
