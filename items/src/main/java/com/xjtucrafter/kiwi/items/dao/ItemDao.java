package com.xjtucrafter.kiwi.items.dao;

import com.xjtucrafter.kiwi.items.bean.Item;

import java.util.List;

/**
 * User: Dash
 * Date: 4/19/16 Time: 9:21 PM
 */
public interface ItemDao {
    int itemAdd(Item item);
    Item itemQuery(String itemName);
    List<Item> itemBatchQuery();
    int itemDelete(int itemID);
    int itemUpdate(Item item);
}
