package com.xjtucrafter.kiwi.account.service.impl;

import com.xjtucrafter.kiwi.account.bean.Account;
import com.xjtucrafter.kiwi.account.dao.AccountDao;
import com.xjtucrafter.kiwi.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: Dash
 * Date: 4/21/16 Time: 9:03 PM
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountDao accountDao;

    @Override
    @Transactional
    public boolean accountAdd(Account account) {
        int result = accountDao.accountAdd(account);
        return result > 0;
    }

    @Override
    public Account accountQuery(String accountPhone) {
        try {
            return accountDao.accountQuery(accountPhone);
        }catch (Exception e){
            return null;
        }
    }

    @Override
    @Transactional
    public boolean accountUpdate(Account account) {
        int result = accountDao.accountUpdate(account);
        return result > 0;
    }

    @Override
    @Transactional
    public boolean accountDelete(String accountPhone) {
        int result = accountDao.accountDelete(accountPhone);
        return result > 0;
    }

    @Override
    public boolean accountQuery(String phone, String pwd) {
        Account account;
        try {
            account = accountDao.accountQuery(phone);
        }catch (Exception e){
            account = null;
        }
        return account != null && pwd.equals(account.getAccountPassword());
    }
}
