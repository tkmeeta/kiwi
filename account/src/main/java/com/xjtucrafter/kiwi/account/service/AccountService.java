package com.xjtucrafter.kiwi.account.service;

import com.xjtucrafter.kiwi.account.bean.Account;

/**
 * User: Dash
 * Date: 4/21/16 Time: 9:03 PM
 */
public interface AccountService {
    boolean accountAdd(Account account);
    Account accountQuery(String accountPhone);
    boolean accountUpdate(Account account);
    boolean accountDelete(String accountPhone);
    boolean accountQuery(String phone, String pwd);
}
