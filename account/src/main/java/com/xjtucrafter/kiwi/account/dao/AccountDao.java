package com.xjtucrafter.kiwi.account.dao;

import com.xjtucrafter.kiwi.account.bean.Account;

import javax.persistence.NoResultException;

/**
 * User: Dash
 * Date: 4/21/16 Time: 9:08 PM
 */
public interface AccountDao {
    int accountAdd(Account account);
    Account accountQuery(String accountPhone) throws NoResultException;
    int accountUpdate(Account account);
    int accountDelete(String accountPhone);
}
