package com.xjtucrafter.kiwi.account.controller;

import com.alibaba.fastjson.JSONObject;
import com.xjtucrafter.kiwi.account.bean.Account;
import com.xjtucrafter.kiwi.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * User: Dash
 * Date: 4/21/16 Time: 8:47 PM
 */
@Controller
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService service;

    /**
     * 用户注册
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public boolean accountAdd(HttpServletRequest request){
        String phone = request.getParameter("account_phone");
        String name = request.getParameter("account_name");
        String pwd = request.getParameter("account_pwd");
        String avatar = request.getParameter("account_avatar_url");

        Account account = new Account();
        account.setAccountPhone(phone);
        account.setAccountName(name);
        account.setAccountPassword(pwd);
        account.setAccountAvatar(avatar);
        return service.accountAdd(account);
    }

    /**
     * 登录
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject accountLogin(HttpServletRequest request){
        JSONObject result = new JSONObject();
        String phone = request.getParameter("account_phone");
        String pwd = request.getParameter("account_pwd");

        if(service.accountQuery(phone, pwd)){
            result.put("message","登录成功");
        }else {
            result.put("message", "登录失败,请输入用户名密码后重试");

        }
        return result;
    }

    /**
     * 查询用户
     */
    @RequestMapping(value = "/{account_phone}", method = RequestMethod.GET)
    @ResponseBody
    public Account accountQuery(@PathVariable("account_phone") String accountPhone){
        return service.accountQuery(accountPhone);
    }


    /**
     * 用户更新
     */
    @RequestMapping(value = "/update/{account_phone}", method = RequestMethod.POST)
    @ResponseBody
    public boolean accountUpdate(@PathVariable("account_phone") String accountPhone,
                                 HttpServletRequest request){
        String name = request.getParameter("account_name");
        String pwd = request.getParameter("account_pwd");
        String avatar = request.getParameter("account_avatar_url");

        Account account = new Account();
        account.setAccountPhone(accountPhone);
        account.setAccountName(name);
        account.setAccountPassword(pwd);
        account.setAccountAvatar(avatar);

        return service.accountUpdate(account);
    }

    /**
     * 删除用户
     */
    @RequestMapping(value = "/{account_phone}", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean accountDelete(@PathVariable("account_phone") String accountPhone){
        return service.accountDelete(accountPhone);
    }
}
