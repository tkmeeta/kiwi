package com.xjtucrafter.kiwi.merchant.controller;

import com.alibaba.fastjson.JSONObject;
import com.xjtucrafter.kiwi.merchant.bean.Merchant;
import com.xjtucrafter.kiwi.merchant.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * User: Dash
 * Date: 4/21/16 Time: 10:50 AM
 */

@Controller
@RequestMapping("/merchant")
public class MerchantController {

    @Autowired
    private MerchantService service;
    /**
     * 商家的添加
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public boolean merchantAdd(HttpServletRequest request){
        String merchantName = request.getParameter("merchant_name");
        String merchantPassword = request.getParameter("merchant_pwd");
        String merchantLicenseUrl = request.getParameter("merchant_license_url");
        String merchantPicUrl = request.getParameter("merchant_pic_url");
        String merchantAddress = request.getParameter("merchant_addr");
        String merchantPhone = request.getParameter("merchant_phone");
        String merchantLegalPerson = request.getParameter("merchant_legal_person");

        Merchant merchant = new Merchant();
        merchant.setMerchantName(merchantName);
        merchant.setMerchantPassword(merchantPassword);
        merchant.setMerchantLicenseUrl(merchantLicenseUrl);
        merchant.setMerchantPicUrl(merchantPicUrl);
        merchant.setMerchantAddress(merchantAddress);
        merchant.setMerchantPhone(merchantPhone);
        merchant.setMerchantLegalPerson(merchantLegalPerson);

        return service.merchantAdd(merchant);
    }



    /**
     * 单一商家查询
     */
    @RequestMapping(value = "/{merchant_name}", method = RequestMethod.GET)
    @ResponseBody
    public Merchant merchantQuery(@PathVariable("merchant_name") String merchantName){
        return service.merchantQuery(merchantName);
    }


    /**
     * 批量商家查询
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public List<Merchant> merchantBatchQuery(){
        return service.merchantBatchQuery();
    }

    /**
     * 商家更新
     */
    @RequestMapping(value = "/update/{merchant_id}", method = RequestMethod.POST)
    @ResponseBody
    public boolean merchantUpdate(HttpServletRequest request,
                                  @PathVariable("merchant_id") int merchantId){
        String merchantName = request.getParameter("merchant_name");
        String merchantPassword = request.getParameter("merchant_pwd");
        String merchantLicenseUrl = request.getParameter("merchant_license_url");
        String merchantPicUrl = request.getParameter("merchant_pic_url");
        String merchantAddress = request.getParameter("merchant_addr");
        String merchantPhone = request.getParameter("merchant_phone");
        String merchantLegalPerson = request.getParameter("merchant_legal_person");
        String isAccepted = request.getParameter("merchant_is_accepted");
        boolean merchantIsAccepted = false;
        if ("true".equals(isAccepted)) {
            merchantIsAccepted = true;
        }

        Merchant merchant = new Merchant();
        merchant.setMerchantID(merchantId);
        merchant.setMerchantName(merchantName);
        merchant.setMerchantPassword(merchantPassword);
        merchant.setMerchantLicenseUrl(merchantLicenseUrl);
        merchant.setMerchantPicUrl(merchantPicUrl);
        merchant.setMerchantAddress(merchantAddress);
        merchant.setMerchantPhone(merchantPhone);
        merchant.setMerchantLegalPerson(merchantLegalPerson);
        merchant.setMerchantIsAccepted(merchantIsAccepted);

        return service.merchantUpdate(merchant);
    }

    /**
     * 商家删除
     */
    @RequestMapping(value = "/{merchant_name}", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean merchantDelete(@PathVariable("merchant_name") String merchantName){
        return service.merchantDelete(merchantName);
    }

    /**
     * 商家登录
     */
    @RequestMapping(value ="/login", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject merchantLogin(HttpServletRequest request){
        JSONObject result = new JSONObject();

        String name = request.getParameter("merchant_name");
        String password = request.getParameter("merchant_pwd");

        if(service.merchantQuery(name, password)){
            result.put("message","登录成功");
        }else{
            result.put("message", "登录失败,请输入用户名密码后重试");
        }
        return result;
    }
}
