package com.xjtucrafter.kiwi.merchant.dao.impl;

import com.xjtucrafter.kiwi.merchant.bean.Merchant;
import com.xjtucrafter.kiwi.merchant.dao.MerchantDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * User: Dash
 * Date: 4/21/16 Time: 2:22 PM
 */
@Repository("merchantDao")
public class MerchantDaoImpl implements MerchantDao{

    @PersistenceContext(name="unitName")
    private EntityManager entityManager;

    @Override
    public int merchantAdd(Merchant merchant) {
        entityManager.persist(merchant);
        return merchant.getMerchantID();
    }

    @Override
    public Merchant merchantQuery(String merchantName) throws NoResultException{
        String jpql = "select merchant from Merchant merchant where merchant.merchantName=:name";
        Merchant merchant = (Merchant) entityManager.createQuery(jpql)
                .setParameter("name", merchantName)
                .getSingleResult();
        return merchant;
    }

    @Override
    public List<Merchant> merchantBatchQuery() throws NoResultException{
        String jpql = "select merchant from Merchant merchant";
        List<Merchant> merchantList = entityManager.createQuery(jpql)
                .getResultList();
        return  merchantList;
    }

    @Override
    public int merchantUpdate(Merchant merchant) {
        String jpql = "update Merchant merchant set " +
                "merchant.merchantName=:name," +
                "merchant.merchantPassword=:password," +
                "merchant.merchantLicenseUrl=:licenseUrl," +
                "merchant.merchantPicUrl=:picUrl," +
                "merchant.merchantAddress=:address," +
                "merchant.merchantPhone=:phone," +
                "merchant.merchantLegalPerson=:legalPerson," +
                "merchant.merchantIsAccepted=:isAccepted where " +
                "merchant.merchantID=:id";
        int result = entityManager.createQuery(jpql)
                .setParameter("name", merchant.getMerchantName())
                .setParameter("password", merchant.getMerchantPassword())
                .setParameter("licenseUrl", merchant.getMerchantLicenseUrl())
                .setParameter("picUrl", merchant.getMerchantPicUrl())
                .setParameter("address", merchant.getMerchantAddress())
                .setParameter("phone", merchant.getMerchantPhone())
                .setParameter("legalPerson", merchant.getMerchantLegalPerson())
                .setParameter("isAccepted", merchant.isMerchantIsAccepted())
                .setParameter("id", merchant.getMerchantID())
                .executeUpdate();
        return result;
    }


    @Override
    public int merchantUpdate(Merchant merchant, boolean isAccepted) {
        String jpql = "update Merchant merchant set " +
                "merchant.merchantIsAccepted=:isAccepted where " +
                "merchant.merchantID=:id";
        int result = entityManager.createQuery(jpql)
                .setParameter("isAccepted", isAccepted)
                .setParameter("id",merchant.getMerchantID())
                .executeUpdate();
        return result;
    }


    @Override
    public int merchantDelete(String merchantName) {
        String jpql = "delete from Merchant merchant where merchant.merchantName=:name";
        int result = entityManager.createQuery(jpql)
                .setParameter("name", merchantName)
                .executeUpdate();
       return result;
    }
}
