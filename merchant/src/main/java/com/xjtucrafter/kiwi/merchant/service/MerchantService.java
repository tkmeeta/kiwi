package com.xjtucrafter.kiwi.merchant.service;

import com.xjtucrafter.kiwi.merchant.bean.Merchant;

import java.util.List;

/**
 * User: Dash
 * Date: 4/21/16 Time: 1:24 PM
 */
public interface MerchantService {
    boolean merchantAdd(Merchant merchant);
    Merchant merchantQuery(String merchantName);
    List<Merchant> merchantBatchQuery();
    boolean merchantUpdate(Merchant merchant);
    boolean merchantDelete(String merchantName);
    boolean merchantQuery(String name, String password);
}
