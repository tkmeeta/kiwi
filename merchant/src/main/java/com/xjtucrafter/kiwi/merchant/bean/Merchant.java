package com.xjtucrafter.kiwi.merchant.bean;

import javax.persistence.*;

/**
 * User: Dash
 * Date: 4/21/16 Time: 10:51 AM
 */
@Entity
@Table(name = "merchant")
public class Merchant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int merchantID;
    private String merchantName;
    private String merchantPassword;
    private String merchantLicenseUrl;
    private String merchantPicUrl;
    private String merchantAddress;
    private String merchantPhone;
    private String merchantLegalPerson;
    private boolean merchantIsAccepted;

    public int getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(int merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantPassword() {
        return merchantPassword;
    }

    public void setMerchantPassword(String merchantPassword) {
        this.merchantPassword = merchantPassword;
    }

    public String getMerchantLicenseUrl() {
        return merchantLicenseUrl;
    }

    public void setMerchantLicenseUrl(String merchantLicenseUrl) {
        this.merchantLicenseUrl = merchantLicenseUrl;
    }

    public String getMerchantPicUrl() {
        return merchantPicUrl;
    }

    public void setMerchantPicUrl(String merchantPicUrl) {
        this.merchantPicUrl = merchantPicUrl;
    }

    public String getMerchantAddress() {
        return merchantAddress;
    }

    public void setMerchantAddress(String merchantAddress) {
        this.merchantAddress = merchantAddress;
    }

    public String getMerchantPhone() {
        return merchantPhone;
    }

    public void setMerchantPhone(String merchantPhone) {
        this.merchantPhone = merchantPhone;
    }

    public String getMerchantLegalPerson() {
        return merchantLegalPerson;
    }

    public void setMerchantLegalPerson(String merchantLegalPerson) {
        this.merchantLegalPerson = merchantLegalPerson;
    }

    public boolean isMerchantIsAccepted() {
        return merchantIsAccepted;
    }

    public void setMerchantIsAccepted(boolean merchantIsAccepted) {
        this.merchantIsAccepted = merchantIsAccepted;
    }

}
