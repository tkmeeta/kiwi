package com.xjtucrafter.kiwi.merchant.dao;

import com.xjtucrafter.kiwi.merchant.bean.Merchant;

import javax.persistence.NoResultException;
import java.util.List;

/**
 * User: Dash
 * Date: 4/21/16 Time: 2:21 PM
 */
public interface MerchantDao {
    int merchantAdd(Merchant merchant);
    Merchant merchantQuery(String merchantName) throws NoResultException;
    List<Merchant> merchantBatchQuery() throws NoResultException;
    int merchantUpdate(Merchant merchant);
    int merchantUpdate(Merchant merchant, boolean isAccepted);
    int merchantDelete(String merchantName);
}
