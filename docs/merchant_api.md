# 商家管理模块API

## 商家的注册
- **请求**

```
POST /merchant/ HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

merchant_name=dsah&merchant_pwd=123&merchant_license_url=licenseurl&merchant_pic_url=pciurl&merchant_addr=addr&merchant_phone=phone&merchant_legal_person=legalperson
```

- 请求体参数列表

|  键                   | 值         | 是否为空 | 数据类型|
| ----------------------|:----------:|-------:| ------:|
| merchant_name         | 商家名称    |  否     | string |
| merchant_pwd          | 注册密码    |  否     | string |
| merchant_license_url  | 营业执照    |  否     | string |
| merchant_pic_url      | 图片地址    |  是     | string |
| merchant_addr         | 商家地址    |  是     | string |
| merchant_phone        | 商家电话    |  是     | string |
| merchant_legal_person | 商家法人    |  是     | string |





- 注册成功响应

```
true
```

- 注册失败响应

```
fasle
```

## 商家登录
- **请求**

```
POST /merchant/login HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

merchant_name=dsah&merchant_pwd=123
```


|  键                   | 值         | 是否为空 | 数据类型|
| ----------------------|:----------:|-------:| ------:|
| merchant_name         | 商家名称    |  否     | string |
| merchant_pwd          | 登录密码    |  否     | string |

- 登录成功响应

```
{
  "message": "登录成功"
}
```

- 登录失败响应

```
{
  "message": "登录失败,请输入用户名密码后重试"
}
```


## 单一商家查询
- **请求**


```

GET /merchant/dsah HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache

```

- 商家存在响应


```
{
  "merchantID": 3,
  "merchantName": "dsah",
  "merchantPassword": "123",
  "merchantLicenseUrl": "licenseurl",
  "merchantPicUrl": "pciurl",
  "merchantAddress": "addr",
  "merchantPhone": "phone",
  "merchantLegalPerson": "legalperson",
  "merchantIsAccepted": false
}
```

- 商家不存在响应


```
Null
```

## 批量商家查询

```
GET /merchant/ HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache

```

- 响应

```
[
  {
    "merchantID": 2,
    "merchantName": "dsah1",
    "merchantPassword": "1234",
    "merchantLicenseUrl": "licenseurl",
    "merchantPicUrl": "pciurl",
    "merchantAddress": "addr",
    "merchantPhone": "phone",
    "merchantLegalPerson": "legalperson",
    "merchantIsAccepted": false
  },
  {
    "merchantID": 3,
    "merchantName": "dsah",
    "merchantPassword": "123",
    "merchantLicenseUrl": "licenseurl",
    "merchantPicUrl": "pciurl",
    "merchantAddress": "addr",
    "merchantPhone": "phone",
    "merchantLegalPerson": "legalperson",
    "merchantIsAccepted": false
  }
]
```

## 删除商家
- **请求** 

```
DELETE /merchant/dsah HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

```

- 删除成功的响应

```
true
```

- 删除失败响应

```
false
```

## 商家信息更改

- **请求**

```
POST /merchant/update/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

merchant_name=dash&merchant_pwd=123&merchant_license_url=aaaa&merchant_pic_url=picurl&merchant_addr=addr&merchant_phone=phone&merchant_legal_person=legalperson&merchant_is_accepted=true
```

- 请求体参数列表

|  键                   | 值         | 是否为空 | 数据类型|
| ----------------------|:----------:|-------:| ------:|
| merchant_name         | 商家名称    |  否     | string |
| merchant_pwd          | 注册密码    |  否     | string |
| merchant_license_url  | 营业执照    |  否     | string |
| merchant_pic_url      | 图片地址    |  否     | string |
| merchant_addr         | 商家地址    |  否     | string |
| merchant_phone        | 商家电话    |  否     | string |
| merchant_legal_person | 商家法人    |  否     | string |
| merchant_is_accepted  | 审核通过    |  否     | boolean|


- 更改成功响应

```
true
```

- 更改失败响应

```
false
```