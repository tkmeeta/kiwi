# kiwi文档
kiwi项目开发文档，小伙伴有什么问题可以查看这里

# 目录
- [模块划分](http://git.oschina.net/DashShen/kiwi/blob/release/docs/achitecture.md)
- [API]
    + [account模块](http://git.oschina.net/DashShen/kiwi/blob/release/docs/account_api.md)
    + [items模块](http://git.oschina.net/DashShen/kiwi/blob/release/docs/items_api.md)
    + [merchant模块](http://git.oschina.net/DashShen/kiwi/blob/release/docs/merchant_api.md)
    + [order模块](http://git.oschina.net/DashShen/kiwi/blob/release/docs/order_api.md)

