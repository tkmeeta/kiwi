# 菜品管理模块API

## 菜品的添加
- **请求**

```
POST /items/ HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

item_name=dash1&item_belong=asd2&item_price=12.1
```

- 请求体参数列表

|  键          | 值         | 是否为空 | 数据类型|
| -------------|:---------:|-------:| ------:|
| item_name    | 菜品名称    |  否    | string |
| item_price   | 菜品价格    |  否    | double |
| item_belong  | 所属商家    |  否    | string |
| item_pic_url | 图片地址    |  是    | string |
| item_desc    | 菜品描述    |  是    | string |



- 添加菜品成功响应

```
true
```

- 添加菜品失败响应

```
fasle
```

## 单一菜品查询
- **请求**


```

GET /items/dash HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache

```

- 菜品存在响应


```
{
  "itemID": 1,
  "itemName": "dash",
  "itemPrice": 12.1,
  "itemBelong": "lrc",
  "itemPicUrl": "/test/url",
  "itemDesc": "test_desc"
}
```

- 菜品不存在响应


```
Null
```

## 批量菜品查询

```
GET /items/ HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache

```

- 响应

```
[
  {
    "itemID": 1,
    "itemName": "dash",
    "itemPrice": 12.1,
    "itemBelong": "lrc",
    "itemPicUrl": "/test/url",
    "itemDesc": "test_desc"
  },
  {
    "itemID": 2,
    "itemName": "dash1",
    "itemPrice": 12.1,
    "itemBelong": "asd2",
    "itemPicUrl": null,
    "itemDesc": null
  }
]
```

## 删除单一菜品
- **请求** 

```
DELETE /items/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

```

- 删除成功的响应

```
true
```

- 删除失败响应

```
false
```

## 菜品更改

- **请求**

```
POST /items/update/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

item_name=dash&item_belong=lrc&item_price=12.1&item_desc=test_desc&item_pic_url=%2Ftest%2Furl
```

- 请求体参数列表

|  键          | 值         | 是否为空 | 数据类型|
| -------------|:---------:|-------:| ------:|
| item_name    | 菜品名称    |  否    | string |
| item_price   | 菜品价格    |  否    | double |
| item_belong  | 所属商家    |  否    | string |
| item_pic_url | 图片地址    |  否    | string |
| item_desc    | 菜品描述    |  否    | string |

- 更改成功响应

```
true
```

- 更改失败响应

```
false
```