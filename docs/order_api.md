# 订单模块api


## 添加购物车


- **请求**

```
POST /orders/ HTTP/1.1
Host: 121.42.182.133:8083
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

account_id=1&merchant_id=1&ordered_items=1&ordered_items=2&ordered_items=3
```


- 请求体参数列表,**ordered_items为菜品id,可以有多个**

|  键                   | 值        | 是否为空 | 数据类型|
| ----------------------|:--------:|--------:| ---:|
| account_id            | 用户id    |  否     | int |
| merchant_id           | 商家id    |  否     | int |
| ordered_items         | 菜品id    |  否     | int |
| ordered_items         | 菜品id    |  否     | int |

- 添加成功，**响应购物车订单id号**

```
1
```


- 添加失败

```
0
```

## 根据用户id查询订单, **account/这里是用户id**

- **请求,account/这里是用户id**

```
GET /orders/account/1 HTTP/1.1
Host: 121.42.182.133:8083
Cache-Control: no-cache

```

- 存在响应

```
[
  {
    "orderID": 1,
    "orderCreateTime": 1461404892000,
    "orderStatus": "payed",
    "itemsJson": "[2,3]",
    "itemsList": [
      2,
      3
    ],
    "merchantId": 1,
    "accountId": 1
  },
  {
    "orderID": 2,
    "orderCreateTime": 1461404895000,
    "orderStatus": "unpayed",
    "itemsJson": "[1,2,3]",
    "itemsList": [
      1,
      2,
      3
    ],
    "merchantId": 1,
    "accountId": 1
  },
  {
    "orderID": 3,
    "orderCreateTime": 1461404896000,
    "orderStatus": "unpayed",
    "itemsJson": "[1,2,3]",
    "itemsList": [
      1,
      2,
      3
    ],
    "merchantId": 1,
    "accountId": 1
  }
]
```

- 不存在响应

```
null
```


## 根据商家id查询订单

- **请求**

```
GET /orders/merchant/1 HTTP/1.1
Host: 121.42.182.133:8083
Cache-Control: no-cache

```

- 存在响应

```
[
  {
    "orderID": 1,
    "orderCreateTime": 1461404892000,
    "orderStatus": "payed",
    "itemsJson": "[2,3]",
    "itemsList": [
      2,
      3
    ],
    "merchantId": 1,
    "accountId": 1
  },
  {
    "orderID": 2,
    "orderCreateTime": 1461404895000,
    "orderStatus": "unpayed",
    "itemsJson": "[1,2,3]",
    "itemsList": [
      1,
      2,
      3
    ],
    "merchantId": 1,
    "accountId": 1
  },
  {
    "orderID": 3,
    "orderCreateTime": 1461404896000,
    "orderStatus": "unpayed",
    "itemsJson": "[1,2,3]",
    "itemsList": [
      1,
      2,
      3
    ],
    "merchantId": 1,
    "accountId": 1
  }
]
```

- 不存在响应

```
null
```


## 用户查看未支付的订单

- **请求**

```
GET /orders/merchant/unpayed/1 HTTP/1.1
Host: 121.42.182.133:8083
Cache-Control: no-cache

```


## 商家查看未支付的订单

- **请求**
```
GET /orders/account/unpayed/1 HTTP/1.1
Host: 121.42.182.133:8083
Cache-Control: no-cache

```

## 根据订单号查询订单

- **请求**

```
GET /orders/1/ HTTP/1.1
Host: 121.42.182.133:8083
Cache-Control: no-cache

```

## 支付订单


- **请求**

```
POST /orders/pay/ HTTP/1.1
Host: 121.42.182.133:8083
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

order_id=1
```


## 更新订单

- **请求**


```
POST /orders/update/1 HTTP/1.1
Host: 121.42.182.133:8083
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

ordered_items=2&ordered_items=3
```

- 请求体参数列表,**ordered_items为菜品id,可以有多个**

|  键                   | 值        | 是否为空 | 数据类型|
| ----------------------|:--------:|--------:| ---:|
| account_id            | 用户id    |  否     | int |
| merchant_id           | 商家id    |  否     | int |
| ordered_items         | 菜品id    |  否     | int |
| ordered_items         | 菜品id    |  否     | int |

## 删除订单

- **请求**

```
DELETE /orders/1 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

order_id=1
```