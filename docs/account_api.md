# 用户管理模块API

## 客户注册
- **请求**

```
POST /account/ HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

account_phone=18862112185&account_name=dash&account_pwd=123&account_avatar_url=avatar_url
```

- 注册成功

```
true
```

- 注册失败
```
false
```


- 请求体参数列表

|  键                   | 值         | 是否为空 | 数据类型|
| ----------------------|:----------:|-------:| ------:|
| account_phone         | 用户电话    |  否     | string |
| account_name          | 用户名字    |  否     | string |
| account_pwd           | 用户密码    |  否     | string |
| account_avatar_url    | 用户头像    |  是     | string |


## 客户登录
- **请求**

```
POST /account/login HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

account_phone=18862112181&account_pwd=123
```


- 请求体参数列表

|  键                   | 值         | 是否为空 | 数据类型|
| ----------------------|:----------:|-------:| ------:|
| account_phone         | 用户电话    |  否     | string |
| account_pwd           | 用户密码    |  否     | string |





- 登录成功

```
{
  "message": "登录成功"
}
```

- 登录失败

```
{
  "message": "登录失败,请输入用户名密码后重试"
}
```


## 客户更新
- **请求**

```
POST /account/update/18862112185 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

account_name=2dash&account_pwd=1232&account_avatar_url=avatar_url2
```


- 请求体参数列表

|  键                   | 值         | 是否为空 | 数据类型|
| ----------------------|:----------:|-------:| ------:|
| account_phone         | 用户电话    |  否     | string |
| account_name          | 用户名字    |  否     | string |
| account_pwd           | 用户密码    |  否     | string |
| account_avatar_url    | 用户头像    |  否     | string |




- 更新成功

```
true
```

- 更新失败

```
false
```

## 客户查询
- **请求**

```
GET /account/18862112185 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache

```


- 客户存在响应

```
{
  "accountID": 1,
  "accountPhone": "18862112185",
  "accountName": "dash",
  "accountPassword": "123",
  "accountAvatar": "avatar_url"
}
```

- 客户不存在响应

```
Null
```
